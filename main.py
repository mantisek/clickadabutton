import pyautogui as p
from pynput import keyboard
import pydirectinput as pd
import time

class Clickadabutton:
    def __init__(self):
        self.BUTTON_IMAGE = "craftbutton.JPG"
        self.hold = 0
        self.target = None
        self.action = None
        self.play()


    def find_button(self, image):
        img_coords = p.locateOnScreen(image, confidence=0.7, grayscale = True)
        if img_coords != None:
            return img_coords
        else:
            print("Craft button not found")
            self.hold = 0

    def move_to_target(self, target):
        point = p.center(target)
        p.moveTo(point.x, point.y)


    def on_press(self, key):
        try:
            print('alphanumeric key {0} pressed'.format(
                key.char))

        except AttributeError:
            print('special key {0} pressed'.format(
                key))
            if key.name == "f1":
                print('f1')
                self.hold = 1
            elif key.name == "f2":
                print('f2')
                self.hold = 0


    def on_release(self, key):
        if key == keyboard.Key.esc:
            # Stop listener
            return False

    def play(self):
        while True:
            listener = keyboard.Listener(
                on_press=self.on_press,
                on_release=self.on_release)
            listener.start()
            if self.hold == 1:
                if self.target == None:
                    self.target = self.find_button(self.BUTTON_IMAGE)
                else:
                    self.move_to_target(self.target)
                    pd.mouseDown()
                    while True:
                        listener = keyboard.Listener(
                            on_press=self.on_press,
                            on_release=self.on_release)
                        listener.start()
                        if self.hold == 0:
                            break
                        time.sleep(1)


            else:
                self.action = None

def main():
    cl = Clickadabutton()

if __name__ == "__main__":
    main()
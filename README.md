# clickadabutton


No more awkwardly resting stuff on your mouse button!

![image](clickadabutton.png)

**What is it?:** A script that finds the craft button using opencv, moves the mouse over to it and holds down the left mouse button.

**Wait, can't you just hold down the space bar?**: I didn't figure this out until after I finished the script, shut up. This is more stylish anyways.

**How do I use it?**:  
If you're new to running random python scripts off the internet:  https://www.quora.com/How-do-I-run-a-Python-script-that-I-downloaded-from-GitHub
(Make sure to get pip working too)

If you're not new, or you just finished doing the stuff for newbies:

1.`git clone https://gitlab.com/mantisek/clickadabutton`

2.`pip install -r requirements.txt`

3.`python3 main.py`


